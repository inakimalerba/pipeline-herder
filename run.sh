#!/bin/bash

set -euo pipefail

cd /code

/usr/bin/redis-server /etc/redis.conf &
echo "PID redis: $!"

celery -A pipeline_herder.worker worker --concurrency=1 -Q matching &
echo "PID matcher: $!"

celery -A pipeline_herder.worker worker --concurrency=1 -Q herding &
echo "PID herder: $!"

FLASK_APP=pipeline_herder.webhook flask run --host 0.0.0.0 --port 5000 &
echo "PID flask: $!"

# Tell the whole process group that it's time to quit.
trap 'trap - SIGINT SIGTERM EXIT && kill -- -$$' SIGINT SIGTERM EXIT

wait
