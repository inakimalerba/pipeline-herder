"""Test the herder module."""
import os
import time
import unittest
import uuid
from unittest import mock
from urllib.parse import urlparse

import gitlab
import yaml

from pipeline_herder import herding
from pipeline_herder import matchers
from pipeline_herder import matching
from pipeline_herder import settings
from pipeline_herder import utils


def cleanup_gitlab(deletable_objects):
    """Remove merge requests, branches and projects from a GitLab instance."""
    for deletable_object in deletable_objects:
        with utils.only_log_exceptions():
            deletable_object.delete()


@mock.patch.multiple(settings,
                     SHORTENER_URL='',
                     HERDER_RETRY_LIMIT=3,
                     HERDER_ACTION='retry',
                     HERDER_RETRY_DELAYS=(0,))
class TestHerder(unittest.TestCase):
    """Test the herder module."""

    def __init__(self, *args, **kwargs):
        """Create a new test case."""
        super().__init__(*args, **kwargs)
        self.projects = []
        self.gl_project = None
        self.gl_project_personal = None

    @classmethod
    def setUpClass(cls):
        """Set up the Gitlab instances."""
        cls.instance_url = os.environ['IT_GITLAB_URL']
        cls.instance_host = urlparse(cls.instance_url).hostname
        cls.parent_project = os.environ['IT_GITLAB_PARENT_PROJECT']
        cls.token_personal = os.environ['IT_GITLAB_PERSONAL_TOKEN']
        cls.token_bot = os.environ['IT_GITLAB_CI_BOT_TOKEN']
        cls.token_herder = os.environ['IT_GITLAB_HERDER_CI_BOT_TOKEN']
        cls.gl_personal = gitlab.Gitlab(
            cls.instance_url, cls.token_personal)
        cls.gl_personal.auth()
        cls.gl_bot = gitlab.Gitlab(
            cls.instance_url, cls.token_bot)
        cls.gl_bot.auth()
        cls.gl_herder = gitlab.Gitlab(
            cls.instance_url, cls.token_herder)
        cls.gl_herder.auth()
        cls.name_herder = cls.gl_herder.user.username

    def tearDown(self):
        """Clean generated artifacts from the Gitlab instance."""
        cleanup_gitlab(self.projects)

    def _pipeline_job(self, gl_pipeline, job_index):
        return self.gl_project.jobs.get(gl_pipeline.jobs.list()[job_index].id)

    def _wait_job(self, gl_job):  # pylint: disable=no-self-use
        print(f'Waiting for job to finish: {gl_job.web_url}')
        for _ in range(120):
            if gl_job.finished_at:
                print(f'Job status: {gl_job.status}')
                return
            time.sleep(1)
            gl_job.refresh()
        raise Exception('Job did not finish')

    @mock.patch('pipeline_herder.matching.submit_retry')
    def _match_job(self, gl_job, submit_retry=None):
        with mock.patch.object(settings, 'GITLAB_TOKENS',
                               {self.instance_host: self.token_herder}):
            matching.process_job(self.instance_host,
                                 self.gl_project.path_with_namespace,
                                 gl_job.id)
        return submit_retry

    def _gl_job(self, job_id, **kwargs):
        """Return a GitLab job by id."""
        return self.gl_project.jobs.get(job_id, **kwargs)

    def _gl_pipeline(self, gl_job, **kwargs):
        """Return the GitLab pipeline to which a job belongs."""
        pipeline_id = gl_job.pipeline['id']
        return self.gl_project.pipelines.get(pipeline_id, **kwargs)

    def _last_job(self, gl_job):
        """Return the last job with the same name."""
        gl_pipeline = self._gl_pipeline(gl_job, lazy=True)
        gl_jobs = [j for j in gl_pipeline.jobs.list(as_list=False)
                   if j.name == gl_job.name]
        return self._gl_job(gl_jobs[-1].id)

    def _retry_job(self, gl_job, should_fail=False, other_user=0):
        """Retry a job with the herder, and do some basic sanity checks."""
        print('Retrying job')
        with mock.patch.object(settings, 'GITLAB_TOKENS',
                               {self.instance_host: self.token_herder}):
            gl_pipeline = self.gl_project.pipelines.get(gl_job.pipeline['id'])
            herding.retry(self.instance_host,
                          self.gl_project.path_with_namespace,
                          gl_job.id)
            gl_jobs_new = [j for j in gl_pipeline.jobs.list(as_list=False)
                           if j.name == gl_job.name and j.id > gl_job.id]

            if should_fail:
                self.assertEqual(len(gl_jobs_new), other_user)
                self.assertTrue(all(j.user['username'] != self.name_herder
                                    for j in gl_jobs_new))
                return None

            job_urls = [j.web_url for j in gl_jobs_new]
            print(f'Started jobs: {", ".join(job_urls)}')
            gl_job_last = self._last_job(gl_job)
            self.assertEqual(len(gl_jobs_new), 1)
            self.assertEqual(gl_jobs_new[0].name, gl_job.name)
            self.assertEqual(gl_jobs_new[0].user['username'], self.name_herder)
            self.assertEqual(gl_jobs_new[0].id, gl_job_last.id)
            return gl_job_last

    def _gitlab_ci_yaml(self, retry=False):  # pylint: disable=no-self-use
        job_template = {
            'image': 'registry.gitlab.com/cki-project/containers/python',
            'tags': ['pipeline-prepare-runner'],
            'only': ['api'],
        }
        if retry:
            job_template['retry'] = 2
        return yaml.safe_dump({
            'stages': ['stage1', 'stage2'],
            '.job': job_template,
            'job1': {
                'extends': '.job', 'stage': 'stage1',
                'script': ['exit 137'],
            },
            'job2': {
                'extends': '.job', 'stage': 'stage2',
                'script': ['exit 0'],
            }
        }, sort_keys=False)

    def _setup_project(self, gitlab_ci_yml):
        path = str(uuid.uuid4())
        namespace_id = self.gl_bot.namespaces.get(self.parent_project).id
        self.gl_project = self.gl_bot.projects.create({
            'path': path,
            'namespace_id': namespace_id,
            'initialize_with_readme': True})
        print(f'Created project {self.gl_project.name}')
        self.gl_project_personal = self.gl_personal.projects.get(
            self.gl_project.id)
        self.projects.append(self.gl_project_personal)
        payload = {
            'file_path': '.gitlab-ci.yml',
            'branch': 'master',
            'content': gitlab_ci_yml,
            'commit_message': 'GitLab CI configuration'}
        self.gl_project.files.create(payload)

    def _trigger_pipeline(self, variables=None):
        """Trigger and return a pipeline."""
        payload = {
            'ref': 'master',
            'variables': [{'key': key, 'value': value}
                          for key, value in (variables or {}).items()]
        }
        return self.gl_project.pipelines.create(payload)

    def test_matcher(self):
        """Check that a job can be matched."""

        self._setup_project(self._gitlab_ci_yaml())
        gl_pipeline = self._trigger_pipeline()
        gl_job = self._pipeline_job(gl_pipeline, 0)
        self._wait_job(gl_job)

        submit_retry = self._match_job(gl_job)
        submit_retry.assert_called_once_with(mock.ANY, matchers.MATCHERS[0], 0)

    def test_retry(self):
        """Check that a job can be retried."""

        self._setup_project(self._gitlab_ci_yaml())
        gl_pipeline = self._trigger_pipeline()
        gl_job = self._pipeline_job(gl_pipeline, 0)
        self._wait_job(gl_job)

        gl_job = self._retry_job(gl_job)
        self._wait_job(gl_job)
        self.assertIsNotNone(gl_job.finished_at)

    def test_retry_limit(self):
        """Check that a job will be retried with a limit."""

        self._setup_project(self._gitlab_ci_yaml())
        gl_pipeline = self._trigger_pipeline()
        gl_job = self._pipeline_job(gl_pipeline, 0)
        self._wait_job(gl_job)

        gl_job = self._retry_job(gl_job)
        self._wait_job(gl_job)
        self.assertIsNotNone(gl_job.finished_at)

        gl_job = self._retry_job(gl_job)
        self._wait_job(gl_job)
        self.assertIsNotNone(gl_job.finished_at)

        gl_job = self._retry_job(gl_job)
        self._wait_job(gl_job)
        self.assertIsNotNone(gl_job.finished_at)

        # only 3 retries + original job
        self._retry_job(gl_job, should_fail=True)

    def test_retry_external_user(self):
        """Check that a job will not be retried with an external user."""

        self._setup_project(self._gitlab_ci_yaml())
        gl_pipeline = self._trigger_pipeline()
        gl_job = self._pipeline_job(gl_pipeline, 0)
        self._wait_job(gl_job)

        # retry the job as a different user
        self.gl_project_personal.jobs.get(gl_job.id).retry()
        gl_job = self._last_job(gl_job)
        self._wait_job(gl_job)

        self._retry_job(gl_job, should_fail=True)

    def test_retry_retriggered(self):
        """Check that a job will not be retried if it was retriggered."""

        self._setup_project(self._gitlab_ci_yaml())
        gl_pipeline = self._trigger_pipeline({'retrigger': 'true'})
        gl_job = self._pipeline_job(gl_pipeline, 0)
        self._wait_job(gl_job)

        self._retry_job(gl_job, should_fail=True)

    def test_retry_action(self):
        """Check that a job will not be retried for HERDER_ACTION=report."""

        # needs to be a context manager as already present on class
        with mock.patch.object(settings, 'HERDER_ACTION', 'report'):
            self._setup_project(self._gitlab_ci_yaml())
            gl_pipeline = self._trigger_pipeline()
            gl_job = self._pipeline_job(gl_pipeline, 0)
            self._wait_job(gl_job)

            self._retry_job(gl_job, should_fail=True)

    def test_retry_gitlab(self):
        """Check that the herder is able to cope with GitLab retries."""

        self._setup_project(self._gitlab_ci_yaml(retry=True))
        gl_pipeline = self._trigger_pipeline()
        gl_job = self._pipeline_job(gl_pipeline, 0)
        self._wait_job(gl_job)

        # will be retried by GitLab
        self._retry_job(gl_job, should_fail=True, other_user=1)
        gl_job = self._last_job(gl_job)
        self._wait_job(gl_job)

        # will be retried by GitLab
        self._retry_job(gl_job, should_fail=True, other_user=1)
        gl_job = self._last_job(gl_job)
        self._wait_job(gl_job)

        # now we can retry one more time
        gl_job = self._retry_job(gl_job)
        self._wait_job(gl_job)
        self.assertIsNotNone(gl_job.finished_at)

        # the 2 retries by GitLab are taking into account
        self._retry_job(gl_job, should_fail=True)
