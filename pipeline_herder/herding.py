"""Pipeline herder herding tasks."""
import traceback

from celery import Celery

from . import utils

app = Celery('pipeline-herder', broker='redis://localhost')


@app.task(queue='herding')
def retry(gitlab_host, project, job_id):
    """Safely retry a job."""
    job = utils.CachedJob(gitlab_host, project, job_id)
    try:
        unsafe_reason = job.is_retry_unsafe()
        if unsafe_reason:
            utils.notify_irc(job, f'Not retried, {unsafe_reason}')
        else:
            job.gl_job.retry()
            utils.notify_irc(job, 'Retried')
    except Exception:  # pylint: disable=broad-except
        summary = traceback.format_exc().split("\n", 1)[-1]
        utils.notify_irc(job, f'Retry failed: {summary}')
