"""Settings read from env variables."""
import json
import os

HERDER_ACTION = os.environ.get('HERDER_ACTION', 'report')
HERDER_RETRY_LIMIT = int(os.environ.get('HERDER_RETRY_LIMIT', '3'))
HERDER_RETRY_DELAYS = [
    int(d) for d in
    os.environ.get('HERDER_RETRY_DELAYS', '0,3,10').split(',')]

SHORTENER_URL = os.environ.get('SHORTENER_URL')
IRCBOT_URL = os.environ.get('IRCBOT_URL')

GITLAB_TOKENS = {
    h: os.environ.get(t)
    for h, t in json.loads(os.environ.get('GITLAB_INSTANCES', '{}')).items()
}
