"""Download pipeline logs and compare against matchers."""
import argparse
import contextlib
import datetime
import glob
import gzip
import io
import json
import os
import pathlib
import re
import readline
import subprocess
import sys
import tempfile
import threading
from concurrent.futures import ThreadPoolExecutor
from distutils.util import strtobool
from unittest import mock
from urllib.parse import urlparse
from urllib.parse import urlunparse

import dateutil.parser
import responses
import yaml
from gitlab import Gitlab

from . import matchers
from . import matching
from . import settings
from . import utils


class Main:
    """Update a local pipeline/job cache and run matchers."""

    def __init__(self):
        """Initialize the instance from the command line arguments."""
        parser = argparse.ArgumentParser(
            description='Analyze matcher coverage.')
        parser.add_argument('--project-url',
                            default=os.environ.get('GITLAB_URL'),
                            help='Project URL on GitLab with the pipelines')
        parser.add_argument('--private-token',
                            default=os.environ.get('GITLAB_TOKEN'),
                            help='GitLab personal access token')
        parser.add_argument('--update', action='store_true',
                            help='Update the pipeline cache')
        parser.add_argument('--check', action='store_true',
                            help='Check matchers against the pipeline cache')
        parser.add_argument('--review', action='store_true',
                            help='Interactively review new pipelines')
        parser.add_argument('--days', default=7, type=int,
                            help='Number of days to go back during update')
        self.args = parser.parse_args()

        self.root = os.path.expanduser('~/.cache/pipeline-herder')
        self.pipelines = os.path.join(self.root, 'pipelines')

        # used in update
        self.host = None
        self.project = None

    @staticmethod
    def _gitlab_values_from_url(project_url):
        parts = urlparse(project_url)
        instance_url = urlunparse(list(parts[0:2]) + [''] * 4)
        host = parts.netloc
        project_name = re.sub(r'(.git)?/*$', '', parts.path[1:])
        return (instance_url, host, project_name)

    def run(self):
        """Run the action specified on the command line."""
        if not (self.args.update or self.args.check or self.args.review):
            print('Specify one of --update/--check/--review')
            sys.exit(1)
        if self.args.update:
            self.run_update()
        if self.args.check:
            self.run_check()
        if self.args.review:
            self.run_review()

    def run_update(self):
        """Update the pipeline cache."""
        instance_url, self.host, project_name = self._gitlab_values_from_url(
            self.args.project_url)
        instance = Gitlab(
            instance_url, private_token=self.args.private_token)
        self.project = instance.projects.get(project_name)

        project_dir = f'{self.pipelines}/{self.host}/{self.project.id}'
        os.makedirs(project_dir, exist_ok=True)
        self._write_json(f'{project_dir}/project.json',
                         self.project.attributes)

        date_from = (datetime.datetime.now().astimezone() -
                     datetime.timedelta(days=self.args.days))

        with ThreadPoolExecutor(max_workers=32) as executor:
            for pipeline in self.project.pipelines.list(as_list=False):
                if dateutil.parser.parse(pipeline.created_at) < date_from:
                    break
                executor.submit(self.update_pipeline, pipeline)

    def update_pipeline(self, pipeline):
        """Process the pipeline."""
        pipeline_prefix = f'{self.host}/{self.project.id}/{pipeline.id}'
        pipeline_dir = f'{self.pipelines}/{pipeline_prefix}'
        pipeline_stamp = f'{pipeline.status}.{pipeline.updated_at}'
        stamp_file = pathlib.Path(f'{pipeline_dir}/{pipeline_stamp}.stamp')
        if stamp_file.exists():
            print(f'Pipeline {pipeline_prefix}: skipping')
            return
        print(f'Pipeline {pipeline_prefix}: processing')
        for old_stamp_name in glob.glob(f'{pipeline_dir}/*.stamp'):
            os.remove(old_stamp_name)
        os.makedirs(pipeline_dir, exist_ok=True)
        self._write_json(f'{pipeline_dir}/pipeline.json', pipeline.attributes)
        self._write_json(f'{pipeline_dir}/variables.json', [
            v.attributes for v in pipeline.variables.list(as_list=False)])

        for job in pipeline.jobs.list(as_list=False):
            print(f'Job {pipeline_prefix}/{job.id}')
            job_dir = f'{pipeline_dir}/{job.name}/{job.id}'
            os.makedirs(job_dir, exist_ok=True)
            for old_stamp_name in glob.glob(f'{job_dir}/*.stamp'):
                os.remove(old_stamp_name)
            job_stamp = f'{job.status}'
            pathlib.Path(f'{job_dir}/{job_stamp}.stamp').touch()
            self._write_json(f'{job_dir}/job.json', job.attributes)
            self._write_text(f'{job_dir}/trace.log', self.project.jobs.get(
                job.id, lazy=True).trace().decode())

            with contextlib.suppress(Exception):
                self._write_bytes(f'{job_dir}/rc', self.project.jobs.get(
                    job.id, lazy=True).artifact('rc'))

        stamp_file.touch()

    def run_check(self):
        """Run matchers against the pipeline cache."""
        count_all = 0
        counts = {}
        for job_dir in glob.glob(f'{self.pipelines}/*/*/*/*/*'):
            matcher_expected = self._match_expected(job_dir)
            # for now, short-circuit if there is no failure
            if not matcher_expected:
                continue
            matcher = self.check_job(job_dir)
            count_all += 1
            counts[matcher] = counts.get(matcher, 0) + 1
        print(f'Total: {count_all}')
        for matcher, count in counts.items():
            print(f'{matcher}: {count / count_all * 100:.1f} ({count})')

    def _load_review_setting(self, what):
        path = pathlib.Path(f'{self.root}/review.yaml')
        if not path.exists():
            return []
        return yaml.safe_load(path.read_text()).get(what, [])

    def _update_review_setting(self, what, item):
        path = pathlib.Path(f'{self.root}/review.yaml')
        config = yaml.safe_load(path.read_text()) if path.exists() else {}
        config.setdefault(what, []).append(item)
        path.write_text(yaml.safe_dump(config))

    def _vim(self, job_dirs, executable='gvim'):
        with tempfile.TemporaryDirectory() as tmpdir:
            files = []
            for job_dir in job_dirs:
                job_id = os.path.basename(job_dir)
                job_log_path = f'{tmpdir}/{job_id}.log'
                pathlib.Path(job_log_path).write_text(
                    self._cleaned_log(f'{job_dir}/trace.log'))
                files.append(job_log_path)
            subprocess.run([executable, '-f'] + files,
                           check=True,
                           stdout=subprocess.DEVNULL)

    def _vimdiff(self, job_dir):
        self._vim(sorted(glob.glob(f'{job_dir}/../*')), executable='gvimdiff')

    def _cleaned_log(self, file_name):
        raw = self._read_text(file_name)
        raw = raw.replace('\x0d', '\n')
        raw = re.sub(r'\x1b.*?[Km]', '', raw)
        raw = re.sub(r'line [0-9]+', '', raw)
        lines = raw.split('\n')
        lines = [line for line in lines
                 if not (line.startswith('section_start') or
                         line.startswith('section_end'))]
        return '\n'.join(lines)

    @staticmethod
    def _review_matcher(expression):
        return utils.Matcher(name=expression, description=expression,
                             messages=re.compile(expression))

    def _run_matchers(self, jobs, all_matchers):
        ignored_jobs = self._load_review_setting('ignored_jobs')
        result = {
            'total': len(jobs),
            'matched': 0,
            'ignored': 0,
            'missing': 0,
            'missing_jobs': [],
            'counts': {m: 0 for m in all_matchers},
            'job_dirs': {m: [] for m in all_matchers},
        }
        for job_dir in jobs:
            if job_dir in ignored_jobs:
                result['ignored'] += 1
                continue
            lines = self._read_text(f'{job_dir}/trace.log').split('\n')
            try:
                matcher = next(m for m in all_matchers
                               if m.check_lines(lines))
                result['matched'] += 1
                result['counts'][matcher] += 1
                result['job_dirs'][matcher].append(job_dir)
            except StopIteration:
                result['missing_jobs'].append(job_dir)
                result['missing'] += 1
        return result

    def _get_stats(self, recovered_jobs, failed_jobs):
        all_matchers = matchers.MATCHERS + [
            self._review_matcher(e)
            for e in self._load_review_setting('expressions')]
        recovered = self._run_matchers(recovered_jobs, all_matchers)
        failed = self._run_matchers(failed_jobs, all_matchers)
        print(f'{"index":5} {"name":60}{"recovered":>10}{"failed":>10}')
        print('-' * 86)
        for key in ('total', 'matched', 'ignored', 'missing'):
            print(f'{"":5} {key[:59]:60}{recovered[key]:10}{failed[key]:10}')
        print('-' * 86)
        for index, matcher in enumerate(all_matchers):
            recovered_count = recovered['counts'][matcher] or ''
            failed_count = failed['counts'][matcher] or ''
            print(f'{index:5} {matcher.name[:59]:60}' +
                  f'{recovered_count:10}{failed_count:10}')
        return recovered['missing_jobs'], all_matchers

    def run_review(self):
        """Interactively review new pipelines."""
        readline.read_init_file()
        all_jobs = glob.glob(f'{self.pipelines}/*/*/*/*/*')
        recovered_jobs = [job_dir for job_dir in all_jobs
                          if self._match_expected(job_dir) is True]
        failed_jobs = [job_dir for job_dir in all_jobs
                       if self._match_expected(job_dir) is False]
        while True:
            missing_jobs, all_matchers = self._get_stats(
                recovered_jobs, failed_jobs)

            response = input('Enter n for next trace, [tf]123 to ' +
                             'inspect true/false positives or q to abort... ')
            if response == 'q':
                break
            if re.match('[tf][0-9]+', response):
                jobs = recovered_jobs if response[0] == 't' else failed_jobs
                review_matcher = all_matchers[int(response[1:])]
                review_results = self._run_matchers(jobs, [review_matcher])
                job_dirs = review_results['job_dirs'][review_matcher]
                threading.Thread(target=self._vim,
                                 args=[job_dirs]).start()
                continue
            if response != 'n':
                continue

            current_job = missing_jobs[0]
            threading.Thread(target=self._vimdiff,
                             args=[current_job]).start()
            response = input('Expression [i=ignore]: ')
            expression = None
            while True:
                if response == 'i':
                    self._update_review_setting('ignored_jobs',
                                                current_job)
                    break
                if response == 'y' and expression:
                    self._update_review_setting('expressions',
                                                expression)
                    break
                expression = response
                print(f'Current expression: {expression}')
                review_matcher = self._review_matcher(expression)
                review_results = self._run_matchers(missing_jobs,
                                                    [review_matcher])
                print(f'Matched: {review_results["matched"]}')
                response = input('Expression [i=ignore, y=accept current]: ')

    @mock.patch.object(settings, 'SHORTENER_URL', '')
    @mock.patch.object(matching, 'submit_retry')
    @responses.activate
    def check_job(self, job_dir,  # pylint: disable=no-self-use
                  submit_retry=None):
        """Run matchers against a cached job."""
        pipeline_dir = f'{job_dir}/../..'
        project_dir = f'{job_dir}/../../..'
        _, host, project, pipeline, _, job = job_dir.rsplit('/', 5)

        base_url = f'https://{host}/api/v4/projects/{project}'
        job_url = f'{base_url}/jobs/{job}'
        pipeline_url = f'{base_url}/pipelines/{pipeline}'

        responses.add(responses.GET, f'{base_url}',
                      json=self._read_json(f'{project_dir}/project.json'))
        responses.add(responses.GET, f'{job_url}',
                      json=self._read_json(f'{job_dir}/job.json'))
        responses.add(responses.GET, f'{job_url}/trace',
                      body=self._read_text(f'{job_dir}/trace.log'))
        responses.add(responses.GET, f'{pipeline_url}',
                      json=self._read_json(f'{pipeline_dir}/pipeline.json'))
        responses.add(responses.GET, f'{pipeline_url}/variables',
                      json=self._read_json(f'{pipeline_dir}/variables.json'))
        responses.add(responses.GET, f'{pipeline_url}/jobs',
                      json=[self._read_json(j) for j in
                            glob.glob(f'{pipeline_dir}/*/*/job.json')])

        with contextlib.redirect_stdout(io.StringIO()):
            matching.process_job(host, project, int(job))
        if not submit_retry.called:
            return None
        return submit_retry.call_args[0][1].name

    def _match_expected(self, job_dir):
        """Return whether a job should be detected by the matchers."""
        failed = pathlib.Path(f'{job_dir}/failed.stamp').exists()
        if not failed:
            return None
        if any(v['key'] == 'retrigger' and strtobool(v['value'])
               for v in self._read_json(f'{job_dir}/../../variables.json')):
            return None

        jobs_same_name = glob.glob(f'{job_dir}/../*')
        if len(jobs_same_name) <= 1:
            return False
        another_success = any(pathlib.Path(f'{j}/success.stamp').exists()
                              for j in jobs_same_name)
        if not another_success:
            return False

        return True

    def _read_json(self, path):
        return json.loads(self._read_text(path))

    def _read_text(self, path):
        return self._read_bytes(path).decode('utf-8')

    def _read_bytes(self, path):  # pylint: disable=no-self-use
        with gzip.open(f'{path}.gz') as zipfile:
            return zipfile.read()

    def _write_json(self, path, data):
        self._write_text(path, json.dumps(data))

    def _write_text(self, path, data):
        self._write_bytes(path, data.encode('utf-8'))

    def _write_bytes(self, path, data):  # pylint: disable=no-self-use
        with gzip.open(f'{path}.gz', 'wb') as zipfile:
            zipfile.write(data)


if __name__ == '__main__':
    Main().run()
