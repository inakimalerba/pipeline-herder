"""Various matchers."""
import re

from . import utils

MATCHERS = [
    utils.Matcher(
        name='code137',
        description='Pod resource exhaustion (exit code 137)',
        messages=['ERROR: Job failed: command terminated with exit code 137',
                  'No space left on device']
    ),
    utils.Matcher(
        name='image-pull',
        description='Failure during image pull',
        messages='ERROR: Job failed: image pull failed',
    ),
    utils.Matcher(
        name='system-failure',
        description='System failure (automatically restarted by GitLab)',
        messages='ERROR: Job failed (system failure)',
    ),
    utils.Matcher(
        name='ceph-s3',
        description='Problem accessing Ceph S3 buckets',
        messages=[
            re.compile(
                r'(download|upload) failed.*s3://' +
                r'(cki|DH-PROD-CKI|DH-SECURE-CKI)' +
                r'.*(Connection was closed before we received a valid' +
                r' response|AccessDenied|Bad Request)'),
            re.compile(
                r'Connection was closed before we received a valid ' +
                r'response from endpoint URL: ' +
                r'"https://s3.upshift.redhat.com/DH-SECURE-CKI'),
        ]
    ),
    utils.Matcher(
        name='network-pip',
        description='Network troubles reaching pip',
        messages=re.compile(
            r'pip._vendor.requests.exceptions.ConnectionError:.*' +
            r'Name or service not known')
    ),
    utils.Matcher(
        name='network-koji',
        description='Network troubles reaching Koji/Brew',
        messages=re.compile(
            r'koji: ConnectionError: .* Name or service not known')
    ),
    utils.Matcher(
        name='network-instance',
        description='Network troubles reaching the GitLab instance',
        messages=re.compile(
            r'ERROR: Uploading artifacts to coordinator.*' +
            r'FATAL: invalid argument', re.DOTALL),
    ),
    utils.Matcher(
        name='network-bugzilla',
        description='Network troubles reaching the Bugzilla instance',
        messages=re.compile(
            r'Error.*for url: https://bugzilla.redhat.com/xmlrpc.cgi'),
    ),
    utils.Matcher(
        name='network-gitlab-com',
        description='Network troubles reaching gitlab.com',
        messages=re.compile(
            r'fatal: unable to access \'https://gitlab.com/cki-project/.*: ' +
            r'Could not resolve host: gitlab.com'),
    )
]
