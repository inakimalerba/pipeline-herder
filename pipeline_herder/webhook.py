"""Flask app for the webhook."""
import os

import flask
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

from . import matching

app = flask.Flask(__name__)

WEBHOOK_SECRET_TOKEN = os.environ.get('WEBHOOK_SECRET_TOKEN')

if app.env == 'production':
    # the DSN is read from SENTRY_DSN
    sentry_sdk.init(
        ca_certs=os.getenv('REQUESTS_CA_BUNDLE'),
        integrations=[FlaskIntegration()]
    )


@app.route('/', methods=['GET'])
def index():
    """Empty index page."""
    return flask.Response(response='OK', status='200')


@app.route('/webhook', methods=['POST'])
def webhook_receiver():
    """Receive information of job changes and put them into the queue."""
    secret_token = flask.request.headers.get('X-Gitlab-Token')
    if WEBHOOK_SECRET_TOKEN and WEBHOOK_SECRET_TOKEN != secret_token:
        return flask.Response(status='403')
    webhook_json = flask.request.get_json()
    matching.process_webhook.delay(webhook_json)
    return flask.Response(response='OK', status='200')


@app.route('/check/<host>/<project>/<int:job>', methods=['GET'])
def message_receiver(host, project, job):
    """Only for debugging."""
    matching.process_job.delay(host, f'cki-project/{project}', job)
    return flask.Response(response='OK', status='200')
