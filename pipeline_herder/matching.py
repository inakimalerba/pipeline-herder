"""Pipeline herder matching tasks."""
from urllib import parse

from celery import Celery

from . import herding
from . import matchers
from . import utils

app = Celery('pipeline-herder', broker='redis://localhost')


@app.task(queue='matching')
def process_job(gitlab_host, project, job_id):
    """Process a job directly specified via host/project/id."""
    return _process(gitlab_host, project, job_id)


@app.task(queue='matching')
def process_webhook(message):
    """Process a job from a GitLab webhook."""
    object_kind = message['object_kind']
    if object_kind != 'build':
        print(f'Ignoring {object_kind} for {message["project_name"]}')
        return
    status = message['build_status']
    if status not in ('success', 'failed'):
        print(f'Ignoring {status} for {message["project_name"]}')
        return
    homepage = parse.urlparse(message['repository']['homepage'])
    _process(homepage[1], homepage[2].lstrip('/'), message['build_id'])


def _process(gitlab_host, project, job_id):
    job = utils.CachedJob(gitlab_host, project, job_id)
    job_id = job.gl_job.id
    for matcher_instance in matchers.MATCHERS:
        description = matcher_instance.description
        if not matcher_instance.check(job):
            continue
        if matcher_instance.action == 'report':
            utils.notify_irc(job, f'Detected {description}')
            return
        if matcher_instance.action == 'retry':
            unsafe_reason = job.is_retry_unsafe()
            if unsafe_reason:
                utils.notify_irc(job, f'Detected {description}, '
                                 f'not retrying: {unsafe_reason}')
                return
            delay = job.retry_delay()
            if delay == 0:
                utils.notify_irc(job, f'Detected {description}, retrying now')
            else:
                utils.notify_irc(job, f'Detected {description}, '
                                 f'retrying in {delay} minutes')
            submit_retry(job, matcher_instance, delay)
        return


def submit_retry(job, matcher_class, delay):  # pylint: disable=unused-argument
    """Submit the suggested actions with the specified delay in minutes."""
    herding.retry.apply_async(job.args(), countdown=delay * 60)
