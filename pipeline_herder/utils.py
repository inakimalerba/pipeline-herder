"""Various utils used across the pipeline herder."""
import contextlib
import re
import traceback
from distutils.util import strtobool

import gitlab
import requests
from cached_property import cached_property

from . import settings


class CachedJob:
    """Cache information about a GitLab job to minimize API calls."""

    def __init__(self, gitlab_host, project, job_id):
        """Initialize a cached job."""
        self._gitlab_host = gitlab_host
        self._project = project
        self._job_id = job_id

    def args(self):
        """Return the arguments to reconstruct the instance."""
        return (self._gitlab_host, self._project, self._job_id)

    @cached_property
    def gl_instance(self):
        """Return the GitLab instance."""
        return gitlab.Gitlab(f'https://{self._gitlab_host}',
                             settings.GITLAB_TOKENS[self._gitlab_host],
                             session=SESSION)

    @cached_property
    def gl_project(self):
        """Return the GitLab project."""
        return self.gl_instance.projects.get(self._project)

    @cached_property
    def gl_pipeline(self):
        """Return the GitLab pipeline."""
        return self.gl_project.pipelines.get(self.gl_job.pipeline['id'])

    @cached_property
    def gl_pipeline_jobs(self):
        """Return all jobs of the GitLab pipeline."""
        return self.gl_pipeline.jobs.list(all=True)

    @cached_property
    def variables(self):
        """Return the GitLab pipeline variables."""
        return {v.key: v.value for v in self.gl_pipeline.variables.list()}

    @cached_property
    def retriggered(self):
        """Return whether the pipeline was retriggered."""
        return strtobool(self.variables.get('retrigger', 'false'))

    @cached_property
    def gl_job(self):
        """Return the GitLab job."""
        return self.gl_project.jobs.get(self._job_id)

    @cached_property
    def trace(self):
        """Return the GitLab job trace."""
        return self.gl_job.trace().decode('utf8').split('\n')

    @cached_property
    def auth_user_id(self):
        """Return the user owning the GitLab connection."""
        instance = self.gl_instance
        if not hasattr(instance, 'user'):
            instance.auth()
        return instance.user.id

    def job_name_count(self):
        """Return the number of jobs in the pipeline with the same name."""
        return len([j for j in self.gl_pipeline_jobs
                    if j.name == self.gl_job.name])

    def retry_delay(self):
        """Return the number of minutes to wait before a retry."""
        delay_index = self.job_name_count() - 1
        if delay_index >= len(settings.HERDER_RETRY_DELAYS):
            return settings.HERDER_RETRY_DELAYS[-1]
        return settings.HERDER_RETRY_DELAYS[delay_index]

    def is_retry_unsafe(self):
        """Check whether a job can be safely retried by the herder.

        Returns None if the job can be retried or a string with the error
        message.
        """
        # do not retry retriggered pipelines
        if self.retriggered:
            return 'retriggered job'

        # each job must have been started by pipeline owner or herder bot
        gl_pipeline = self.gl_pipeline
        allowed_users = (gl_pipeline.user['id'], self.auth_user_id)
        external_users = set(j.user['username'] for j in self.gl_pipeline_jobs
                             if j.user['id'] not in allowed_users)
        if external_users:
            return f'external users interfered: {", ".join(external_users)}'

        # there should be no newer job with the same name
        newer_jobs = [f'J{j.id}' for j in self.gl_pipeline_jobs
                      if j.name == self.gl_job.name and j.id > self.gl_job.id]
        if newer_jobs:
            return f'job restarted externally as {", ".join(newer_jobs)}'

        # only retry up to a maximum number of times
        if self.job_name_count() > settings.HERDER_RETRY_LIMIT:
            return f'maximum of {settings.HERDER_RETRY_LIMIT} retries reached'

        # only retry if the herder is configured to do so
        if settings.HERDER_ACTION != 'retry':
            return f'HERDER_ACTION={settings.HERDER_ACTION}'

        return None


class Matcher:  # pylint: disable=too-few-public-methods
    """Base class for matchers."""

    # pylint: disable=too-many-arguments
    def __init__(self, name, description, messages,
                 action='retry', tail_lines=100):
        """Initialize the matcher."""
        self.name = name
        self.description = description
        self.action = action
        self.messages = messages if isinstance(messages, list) else [messages]
        self.tail_lines = tail_lines

    @staticmethod
    def _check_lines(message, lines):
        """Check lines for a match."""
        if isinstance(message, re.Pattern):
            return message.search('\n'.join(lines))
        return any(message in line for line in lines)

    def check_lines(self, lines):
        """Check lines for a match."""
        return any(self._check_lines(m, lines) for m in self.messages)

    def check(self, job: CachedJob):
        """Check status/log for a match."""
        if job.gl_job.status != 'failed':
            return False

        return self.check_lines(job.trace[-self.tail_lines:])


def _print_request(response,
                   *args, **kwargs):  # pylint: disable=unused-argument
    print(f'requests: {response.url}')


@contextlib.contextmanager
def only_log_exceptions(exceptions=Exception):
    """Log, but ignore exceptions."""
    try:
        yield
    except exceptions:  # pylint: disable=broad-except
        traceback.print_exc()


def shorten_url(url):
    """Attempt to shorten a URL."""
    with only_log_exceptions():
        if settings.SHORTENER_URL:
            response = SESSION.post(settings.SHORTENER_URL, data={'url': url})
            response.raise_for_status()
            url = response.text.strip()
    return url


def notify_irc(job: CachedJob, notification):
    """Send a message to the IRC bot."""
    with only_log_exceptions():
        status = job.gl_job.status
        name = job.gl_job.name
        job_id = job.gl_job.id
        pipeline_id = job.gl_pipeline.id
        retriggered = ' (retriggered)' if job.retriggered else ''
        url = shorten_url(job.gl_job.web_url)
        message = (f'🤠 P{pipeline_id} J{job_id}{retriggered} {name} {status}:'
                   f' {notification} {url}')
        print(message)
        if settings.IRCBOT_URL:
            SESSION.post(settings.IRCBOT_URL, json={'message': message})


SESSION = requests.Session()
SESSION.headers.update({'User-Agent': 'pipeline-herder'})
SESSION.hooks = {'response': _print_request}
