"""Test the detection of pod resource exhaustion (exit code 137)."""
import unittest
from unittest import mock

import responses

from pipeline_herder import matchers
from pipeline_herder import matching
from pipeline_herder import settings


@mock.patch.object(settings, 'GITLAB_TOKENS', {'host': 'token'})
@mock.patch.object(settings, 'SHORTENER_URL', '')
@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
class TestCase(unittest.TestCase):
    """Test the detection of pod resource exhaustion (exit code 137)."""

    user = {
        "id": 2,
    }

    project_project = {
        "id": "project",
        "path_with_namespace": "project",
    }

    pipeline_524445 = {
        "id": 524445,
        "user": {
            "id": 1,
        }
    }

    variables_524445 = []

    jobs_524445 = [{
        "id": 764828,
        "name": "build x86_64",
        "user": {
            "id": 1,
        },
    }]

    job_764828 = {
        "id": 764828,
        "status": "failed",
        "stage": "build",
        "name": "build x86_64",
        "pipeline": {
            "id": 524445,
        },
        "user": {
            "id": 1,
            "username": "user",
        },
        "web_url": "https://host.name/",
    }

    trace_764828 = (
        '... snip ...'
        '%_rpmdir /builds/cki-project/cki-pipeline/workdir/rpms'
        'section_end:1586362335:build_script'
        '[0Ksection_start:1586362335:after_script'
        '[0K[0K[36;1mRunning after_script[0;m'
        '[0;msection_end:1586362335:after_script'
        '[0Ksection_start:1586362335:upload_artifacts_on_failure'
        '[0K[0K[36;1mUploading artifacts for failed job[0;m'
        '[0;msection_end:1586362335:upload_artifacts_on_failure'
        '[0K[31;1mERROR: Job failed: command terminated with exit code 137'
        '[0;m')

    @responses.activate
    @mock.patch('pipeline_herder.matching.submit_retry')
    def test_exit_137(self, submit_retry: mock.MagicMock):
        """Test the detection of pod resource exhaustion (exit code 137)."""
        base_url = 'https://host/api/v4/projects/project'
        responses.add(responses.GET, 'https://host/api/v4/user',
                      json=self.user)
        responses.add(responses.GET, f'{base_url}',
                      json=self.project_project)
        responses.add(responses.GET, f'{base_url}/jobs/764828',
                      json=self.job_764828)
        responses.add(responses.GET, f'{base_url}/jobs/764828/trace',
                      body=self.trace_764828)
        responses.add(responses.GET, f'{base_url}/pipelines/524445',
                      json=self.pipeline_524445)
        responses.add(responses.GET, f'{base_url}/pipelines/524445/variables',
                      json=self.variables_524445)
        responses.add(responses.GET, f'{base_url}/pipelines/524445/jobs',
                      json=self.jobs_524445)

        matching.process_job('host', 'project', 764828)
        submit_retry.assert_called_once_with(
            mock.ANY, matchers.MATCHERS[0], 0)
